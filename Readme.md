#TwoPods

The TwoBulls iOS commons framework for (y)our convenience.

### Usage

To use the master branch of the repo:  
`pod 'TwoPods', :git => 'git@bitbucket.org:twobulls/TwoPods.git'`

To use a different branch of the repo:  
```pod 'TwoPods', :git => 'git@bitbucket.org:twobulls/TwoPods.git', :branch => 'dev'```

To use a tag of the repo:  
```pod 'TwoPods', :git => 'git@bitbucket.org:twobulls/TwoPods.git', :tag => '0.1.0'```

Or specify a commit:  
```pod 'TwoPods', :git => 'git@bitbucket.org:twobulls/TwoPods.git', :commit => '082f8319af'```

###Contribute

If you have a reusable component and want to reuse it different projects but don't want to copy/paste? Feel free to add it to one of the existing module or create a new one if none of the existing modules is applicable.  
For contributions please checkout `git@bitbucket.org:twobulls/TwoPods.git` and create a feature branch using:  

- [git-flow](http://danielkummer.github.io/git-flow-cheatsheet/)  
- Create tests where appropriate  
- Run `pod spec lint` (Yes, Do it!)  

###License

MIT License. See LICENSE file in the project root.  


###Changelog

0.1.0 First Release  

0.1.1 Added modulemaps to support CommonCrypto  

0.1.2 Fix the Podspec file to pass strict lint checking  

0.1.3 Fix class, function, member access  

0.1.4 Added:  
- Array subscript for getting Element at index: array[safe: 0] without risk of out of bounds exceptions  
- NumberFormatting for Int, CGFloat, Float, Double  
- String subscript for getting one particular Character or String or Range of substring  
- Infix operator for easy background task scheduling with ~> e.g.: `{ /* closure that executes on serial queue */ } ~> { /* closure that runs on main queue */ }`  
- Rounding double to nearest (roundUp(), roundDown() or just round(_:toNearest:)  
- dbg_print()
- UIApplication.applicationDocumentsDirectory() to get the documents directory for the running application  