//
//  StringValidationTests.swift
//  TwoPods
//
//  Created by Carsten Witzke on 29/04/16.
//  Copyright © 2016 TwoBulls. All rights reserved.
//

import XCTest

@testable import TwoPods

class StringValidationTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testValidEmails() {
        // source: https://en.wikipedia.org/w/index.php?title=Email_address
        let valid = [
            "valid@example.org",
            "disposable.style.email.with+symbol@example.com",
            "valid+too@example.com",
            "valid+too@edu.example.com",
            "valid@example.museum",
            "valid@example.com",
            "#!$%&'*+-/=?^_`{}|~@example.org",
            //"\"very.(),:;<>[]\".VERY.\"very@\\ \"very\".unusual\"@strange.example.com", // no, i'm not wrinting a regex to match this stuff…
            //"user@com", technically valid but not really existing in the wild
            //"user@localserver"
        ]
        
        for string in valid {
            XCTAssertTrue(string.isEmail, "\(string) should be a valid email")
        }
    }
    
    func testInValidEmails() {
        // source: https://en.wikipedia.org/w/index.php?title=Email_address
        let invalid = [
            "",
            " ",
            "valid_example.com",
            "valid+too@example@example.com",
            "a\"b(c)d,e:f;g<h>i[j\\k]l@example.com",
            "email@with.another email@following.com",
            "valid@example.museum ",
            " valid@example.museum"
        ]
        
        for string in invalid {
            XCTAssertFalse(string.isEmail, "\(string) should be an invalid email")
        }
    }

}
