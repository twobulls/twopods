//
//  String+MD5_Test.swift
//  TwoPods
//
//  Created by Arjan Duijzer on 01/04/16.
//  Copyright © 2016 TwoBulls. All rights reserved.
//

import XCTest

@testable import TwoPods

class StringMD5Tests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testDataToMD5() {
        let textData: NSData = "Hallo".dataUsingEncoding(NSUTF8StringEncoding)!
        let expectedHash = "d1bf93299de1b68e6d382c893bf1215f"
        
        XCTAssertEqual(expectedHash, textData.MD5().hexString)
    }
    
    func testDataToSHA1() {
        let textData: NSData = "Hallo".dataUsingEncoding(NSUTF8StringEncoding)!
        let expected = "59D9A6DF06B9F610F7DB8E036896ED03662D168F"
        
        XCTAssertEqual(expected, textData.SHA1().hexString.uppercaseString)
    }
    
    func testStringToMD5() {
        let text = "Hallo"
        let expectedHash = "d1bf93299de1b68e6d382c893bf1215f"
        
        XCTAssertEqual(expectedHash, text.MD5())
    }
    
    func testStringToSHA1() {
        let expected = "59D9A6DF06B9F610F7DB8E036896ED03662D168F"
        let text = "Hallo"
        
        XCTAssertEqual(expected, text.SHA1().uppercaseString)
    }
    
    func testEmptyStringToMD5() {
        let expected = "d41d8cd98f00b204e9800998ecf8427e"
        let text = ""
        
        XCTAssertEqual(expected, text.MD5())
    }

}
