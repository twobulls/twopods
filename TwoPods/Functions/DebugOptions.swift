//
//  DebugOptions.swift
//  TwoPods
//
//  Created by Arjan Duijzer on 11/04/16.
//  Copyright © 2016 TwoBulls. All rights reserved.
//

import Foundation

/// Print Swift.print logging when DEBUG_SWIFT environmental variable is set
/// By default it prints the File->Function:Line - Before the given items to log
public func dbg_print(items: Any..., function: String = #function, file: String = #file, line: Int = #line) {

#if DEBUG_SWIFT
    
    let startIdx = items.startIndex
    let endIdx = items.endIndex

    for idx in startIdx ..< endIdx {
        Swift.print("\(file)->\(function):\(line) - \(items[idx])", separator: " ", terminator: "\n")
    }

#endif
}

/// Disable print log messages for non-debug builds
public func print(items: Any..., separator: String = " ", terminator: String = "\n") {
    #if DEBUG_SWIFT
        Swift.print("Don't use print(), but dbg_print()")
    #endif
}
