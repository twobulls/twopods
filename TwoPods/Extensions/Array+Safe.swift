//
//  Array+Safe.swift
//  TwoPods
//
//  Created by Adriaan on 28/04/16.
//  Copyright © 2016 TwoBulls. All rights reserved.
//

import Foundation

public extension Array {
    public subscript(safe index: Int) -> Element? {
        return indices ~= index ? self[index] : nil
    }
}