//
//  JSONDeserialization.swift
//  TwoPods
//
//  Created by Arjan Duijzer on 01/04/16.
//  Copyright © 2016 TwoBulls. All rights reserved.
//

import Foundation

public extension String {
    public var deserializedJSON: NSDictionary? {
        if let data = self.dataUsingEncoding(NSUTF8StringEncoding) {
            do {
                if let object = try NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments) as? NSDictionary {
                    return object
                }
            } catch {}
        }
        return nil
    }
}
