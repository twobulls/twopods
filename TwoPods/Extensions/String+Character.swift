//
//  String+Character.swift
//  TwoPods
//
//  Created by Adriaan on 12/05/16.
//  Copyright © 2016 TwoBulls. All rights reserved.
//

import Foundation

/// Getting a Character or String at a particular Index
/// or get a substring with Range: i ... end
public extension String {

    public subscript(i: Int) -> Character {
        return self[self.startIndex.advancedBy(i)]
    }

    public subscript(i: Int) -> String {
        return String(self[i] as Character)
    }

    public subscript(r: Range<Int>) -> String {
        let start = startIndex.advancedBy(r.startIndex)
        let end = start.advancedBy(r.endIndex - r.startIndex)
        return self[Range(start ..< end)]
    }
}
