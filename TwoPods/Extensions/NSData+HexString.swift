//
//  Data+HexString.swift
//  TwoPods
//
//  Created by Arjan Duijzer on 30/03/16.
//  Copyright © 2016 TwoBulls. All rights reserved.
//

import Foundation

public extension NSData {
    public var hexString: String {
        // "Array" of all bytes:
        let bytes = UnsafeBufferPointer<UInt8>(start: UnsafePointer(self.bytes), count:self.length)
        // Array of hex strings, one for each byte:
        let hexBytes = bytes.map() { String(format: "%02hhx", $0) }
        // Concatenate all hex strings:
        return hexBytes.joinWithSeparator("")
    }
}