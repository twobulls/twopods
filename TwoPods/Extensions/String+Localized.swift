//
//  String+Localized.swift
//  TwoPods
//
//  Created by Arjan Duijzer on 24/03/16.
//  Copyright © 2016 TwoBulls. All rights reserved.
//

import Foundation

public extension String {
    public var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: NSBundle.mainBundle(), value: "", comment: "")
    }
}