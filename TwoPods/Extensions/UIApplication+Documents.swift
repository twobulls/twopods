//
//  UIApplication+Documents.swift
//  TwoPods
//
//  Created by Adriaan on 06/06/16.
//  Copyright © 2016 TwoBulls. All rights reserved.
//

import UIKit

public extension UIApplication {
    /// Returns the URL to the application's Documents directory.
    public class func applicationDocumentsDirectory() -> NSURL? {
    
        let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        return urls.count > 0 ? urls.last : nil
    }
}
