//
//  NSDictionary+JSON.swift
//  TwoPods
//
//  Created by Arjan Duijzer on 31/03/16.
//  Copyright © 2016 TwoBulls. All rights reserved.
//

import Foundation

public extension NSDictionary {
    public var JSONSerialized: String? {
        do {
            #if DEBUG_SWIFT
                let writeOption = NSJSONWritingOptions.PrettyPrinted
            #else
                let writeOption = NSJSONWritingOptions(rawValue:0)
            #endif
            
            let jsonData = try NSJSONSerialization.dataWithJSONObject(self, options: writeOption)
            if let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as? String {
                return jsonString
            }
            
        } catch {}
        return nil
    }
}